# Loan Ledger

## Quickstart

From the command line do:

```
git clone https://bitbucket.org/atifsaddique/loanledger.git
cd loanledger
gradle build
gradle run
```

The application should  be accessible from the browser at http://localhost:8080


## IDE setup

For setting up the development environment in an IDE do the following:

1. Download and install Java 8 JDK 
2. Make sure you have an Eclipse with  installed (preferably [STS](http://spring.io/sts)).
3. Make sure Gradle IDE plugin from Springsource is installed
4. Import the checked out code through *File > Import > Gradle Project*


## More details

- [Spring Boot](http://github.com/spring-projects/spring-boot)
- [Spring Data JPA](http://github.com/spring-projects/spring-data-jpa)
- [Angular JS](https://angularjs.org)