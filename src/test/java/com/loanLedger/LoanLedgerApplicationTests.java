package com.loanLedger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;


import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LoanLedgerApplication.class)
@WebAppConfiguration
public class LoanLedgerApplicationTests {
	
	@Test
	public void testDomain() {

	}

}
