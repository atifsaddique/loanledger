/**
 * 
 */
package com.loanLedger.user.application;

import com.loanLedger.user.domain.model.Item;

/**
 * @author atif
 *
 */
public interface IUserService {
    void saveItem(String username, Item item) throws IllegalArgumentException;
}
