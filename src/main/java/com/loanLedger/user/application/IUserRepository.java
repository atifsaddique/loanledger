/**
 * 
 */
package com.loanLedger.user.application;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.loanLedger.user.domain.model.User;

/**
 * @author atif
 *
 */
public interface IUserRepository extends JpaRepository<User, Long> {
 
	@Query("select user from User user where user.email =:email")
    User getUserByEmail(@Param("email") String email);
	
}
