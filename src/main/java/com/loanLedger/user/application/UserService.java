/**
 * 
 */
package com.loanLedger.user.application;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.loanLedger.user.domain.model.Borrower;
import com.loanLedger.user.domain.model.Item;
import com.loanLedger.user.domain.model.ItemType;
import com.loanLedger.user.domain.model.Reminder;
import com.loanLedger.user.domain.model.ReminderType;
import com.loanLedger.user.domain.model.User;

/**
 * @author atif
 *
 */
@Service
public class UserService implements IUserService {
	
	@Inject
	IUserRepository userRepository;

	
	public void addUser() {
		User user=new User("atif","atif@aurorasolutions.io","+923467374884","123456");
		user.setId(2l);
		List<Item> items=new ArrayList<Item>();
		Reminder reminder1=new Reminder(true,new Date(), ReminderType.SMS);
		Reminder reminder2=new Reminder(false,new Date(), ReminderType.Email);
		Borrower borrower1=new Borrower("Hazim","hazim@aurorasolutions.io","0346-74754884");
		Borrower borrower2=new Borrower("Zain","zain@aurorasolutions.io","+9234574754884");
		
		Item item1=new Item("Bag", "laptop bag",ItemType.Stuff, new Date());
		item1.setBorrower(borrower1);
		item1.setReminder(reminder1);
		//item1.setUser(user);
		
		Item item2=new Item("Money", "Money borrowed for books",ItemType.Cash, new Date());
		item2.setBorrower(borrower2);
		item2.setReminder(reminder2);
		//item2.setUser(user);
		
		items.add(item1);
		items.add(item2);

		user.setItems(items);
		
		User user1=userRepository.saveAndFlush(user);
        System.out.println(user1.getName());
	}
	
	@Override
	public void saveItem(String username, Item item) throws IllegalArgumentException {
		if(username==null || item== null) {
			throw new IllegalArgumentException("invalid data");
		}
		User user=userRepository.getUserByEmail(username);
		item.setUser(user);
		user.getItems().add(item);
		userRepository.saveAndFlush(user);
	}
	
	public void deleteItem(String username ,Long itemId) throws IllegalArgumentException {
		if(itemId==null) {
			throw new IllegalArgumentException("invalid data");
		}
		User user=userRepository.getUserByEmail(username);
		List<Item> items=user.getItems();
		Item item=null;
		for(Item item1:items) {
			if(item1.getId().equals(itemId)) {
				item=item1;
				break;
			}
		}
		if(item!=null) {
			user.getItems().remove(item);
			userRepository.saveAndFlush(user);
		}
	}
}
