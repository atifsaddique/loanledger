/**
 * 
 */
package com.loanLedger.user.ports;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.loanLedger.user.application.IUserRepository;
import com.loanLedger.user.application.IUserService;
import com.loanLedger.user.domain.model.Item;

/**
 * @author atif
 *
 */

@RestController
@RequestMapping("/api")
public class UserResource {

	@Inject
	IUserRepository userRepository;
	
	@Inject
	IUserService userService;

	@RequestMapping(value = "/v1/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object getUsers() {
		return userRepository.findAll();
	}

	@RequestMapping(value = "/v1/saveItem", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object saveItem(@RequestBody Item item,
			@RequestParam(value = "username", required = true) String username) {
            try {
				userService.saveItem(username, item);
			} catch (Exception e) {
				return e.getMessage();
			}
            return "Entity saved successfully";
	}
	
	@RequestMapping(value = "/v1/deleteItem", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object deleteItem() {
		return userRepository.findAll();
	}

}
