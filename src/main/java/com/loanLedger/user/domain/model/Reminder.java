/**
 * 
 */
package com.loanLedger.user.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author atif
 *
 */

@Entity
@Table(name = "T_Reminder")
public class Reminder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4249808328607820592L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Type(type="yes_no")
	private Boolean autoReminder=false;
	
	@Column(name = "date")
	@Type(type="date")
	private Date date;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	private ReminderType type=ReminderType.SMS;
	
	@OneToOne(mappedBy="reminder")
	private Item item;
	
	public Reminder() {
	}

	public Reminder(Boolean autoReminder, Date date, ReminderType type) {
		super();
		this.autoReminder = autoReminder;
		this.date = date;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAutoReminder() {
		return autoReminder;
	}

	public void setAutoReminder(Boolean autoReminder) {
		this.autoReminder = autoReminder;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ReminderType getType() {
		return type;
	}

	public void setType(ReminderType type) {
		this.type = type;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	
}
